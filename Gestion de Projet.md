🌍️ GESTION DE PROJET: CARTOGRAPHIE 🌍️

- Décider du contenu / sélection des données ----> 1/2h
- Scénario utilisateur ----> 2h
- Wireframe / maquette globale ----> 2h

- HTML ----> 1/2 j
- CSS ----> 1/2j
- Leaflet (carte intéractive) ---->2j (intégrer la carte (=appeler l'API), fixer les points GPS, intéragir avec les fonctionalités)
- Récupérer les données (JSON) ---->1/2j
- Webpack import ----> fil rouge
- Mise en prod / correction bugs ---> 1j




🖥️ SCENARIO UTILISATEUR: 🖥️


1) Points de vente STAC 🚍️
2) Parkings Chambéry 🚙️

Cible: visiteurs et chambériens

2 boutons : TC ou VL

A- TC:🚍️
- Choisir la commune (liste)
- Cliquer pour afficher la carte et les résultats
- Repérer les points de résultat sur la carte
- Cliquer sur le marqueur : adresse / libellé / coordoonnées (tel, mail...)
- Lien vers réseaux sociaux

B- VL:🚙️
- Choisir la commune (liste)
- Cliquer pour afficher la carte et les résultats
- Repérer les points de résultat sur la carte
- Cliquer sur le marqueur : adresse / délégataires / coordoonnées (tel, mail, site...)
- Lien vers réseaux sociaux 










