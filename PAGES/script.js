let map;

function init() {
  map = L.map('mapid').setView([45.564601, 5.917781], 14);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);



  //! SELECT MAPS

  var baselayers = {

    OSM: L.tileLayer('http://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png'),
    ESRI: L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'),
    OrthoRM: L.tileLayer.wms('https://public.sig.rennesmetropole.fr/geoserver/ows?', {
      layers: 'raster:ortho2014',
      opacity: 0.1
    }),
    Ancien: L.tileLayer.wms('http://geobretagne.fr/geoserver/photo/wms?', {
      layers: 'satellite-ancien'
    }),

  };
  baselayers.OrthoRM.addTo(map);

  var Plot = L.tileLayer.wms('http://mapsref.brgm.fr/wxs/refcom-brgm/refign', {
    layers: 'PARVEC_BATIMENT',
    format: 'image/png',
    transparent: true
  }).addTo(map);

  var Cadastral = L.tileLayer.wms('http://geobretagne.fr/geoserver/cadastre/wms', {
    layers: 'CP.CadastralParcel',
    format: 'image/png',
    transparent: true
  }).addTo(map);

  var Roads = L.tileLayer.wms('https://public.sig.rennesmetropole.fr/geoserver/ows?', {
    layers: 'ref_rva:vgs_troncon_domanialite',
    format: 'image/png',
    transparent: true
  });

  var data = {
    "Parcelbati": Plot,
    "Cadastre": Cadastral,
    "Routes": Roads
  };

  L.control.layers(baselayers, data, {
    collapsed: false
  }).addTo(map);

  L.control.scale().addTo(map);




  let p = document.getElementById("myLocation");
  p.onclick = currentLocation;
};

window.addEventListener("load", function () {
  init()

});



//! LOCATION POINT


function currentLocation() {

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition((function (position) {

      let newmarker = L.marker([position.coords.latitude, position.coords.longitude], {})
        .addTo(map);

      let circle = L.circle([45.5899034, 5.9118613], {
          color: 'red',
          fillColor: '#f03',
          fillOpacity: 0.5,
          radius: 500
        })
        .addTo(map);


      map.on('click', function () {
        map.removeLayer(circle);
      });

      let logo = '<img src="/home/genoux/Projets/CARTO/Projet/cartographie/PAGES/IMAGES/logoville.png" height="110px" width="120px"/>';
      newmarker.bindPopup("Ma position :<br> Latitude : " + position.coords.latitude + ',<br>Longitude ' + position.coords.longitude + '<br>' + '<br>' + logo).openPopup();
      map.on('click', function () {
        map.removeLayer(newmarker);
      });
    }));

  } else {
    alert("La géolocalisation n'est pas supportée par ce navigateur.");
  }
}



//! function and fetch initiate

function loadData(url, callback) {
  fetch(url)
    .then(function (response) {
      return response.json();
    })
    // parsed json data with fetch here
    .then(function (data) {
      callback(data);
    });
}

//! display only Chambery parkings informations

function getData(data) {

  let properties = data.records;
  let parkinglogo = '<img src="/home/genoux/Projets/CARTO/Projet/cartographie/PAGES/IMAGES/Parking.png" height="80px" width="80px"/>';
  for (let i = 0; i < properties.length; i++) {
    let marker = L.marker(properties[i].fields.coordonnees_gps)
    marker.bindPopup('<b>' + properties[i].fields.nom_park + '<br/>' + properties[i].fields.adresse + '</b>' + '<br/>' + properties[i].fields.acces + '<br/>' + properties[i].fields.capacite + '<br/>' + properties[i].fields.delegataire + '<br/>'  + '<br/>'+ parkinglogo)
      .addTo(map);
    document.getElementById("resetpark").addEventListener("click", function () {
      map.removeLayer(marker);
    });
  }
};


document.getElementById("chambery").addEventListener("click", function () {

  loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&facet=delegataire&facet=secteur&facet=type_park&facet=remarque', getData);
});


document.getElementById("nothing").addEventListener("click", function () {
  alert("Aucune information ne répond à votre requête");
});