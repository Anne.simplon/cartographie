//! ITINERARY


let routeLayer, dir;

function itinerary() {
  dir = MQ.routing.directions();

  routeLayer = MQ.routing.routeLayer({
    directions: dir,
    fitBounds: true,
    ribbonOptions: {
      draggable: false,
      ribbonDisplay: {
        color: '#5882FA',
        opacity: 0.7
      },
      widths: [10, 15, 10, 15, 10, 13, 10, 12, 10, 11, 10, 11, 10, 12, 10, 14, 10]
    },

    altRibbonOptions: {
      show: true,
      ribbonDisplay: {
        color: '#F78181',
        opacity: 0.7
      },
      hoverDisplay: {
        color: 'red',
        opacity: 0.6
      }
    }
  });

  dir.route({
    maxRoutes: 5,
    timeOverage: 25,
    locations: [
      'chambery',
      ' St Alban de Montbel'
    ]
  });

  map.addLayer(routeLayer);

 
}
//  document.getElementById("roadway").addEventListener("click", function () {
//     itinerary();
  
//   });

  itinerary();






// let
//     _firstLatLng,
//     _firstPoint,
//     _secondLatLng,
//     _secondPoint,
//     _distance,
//     _length,
//     _polyline;

// marker.on('mouseover', function (e) {
//     if (_firstLatLng) {
//         _firstLatLng = e.latlng;
//         _firstPoint = e.layerPoint;
//         L.marker(_firstLatLng).addTo(map).bindPopup('Point A<br/>' + e.latlng + '<br/>' + e.layerPoint).openPopup();
//     } else {
//         _secondLatLng = e.latlng;
//         _secondPoint = e.layerPoint;
//         L.marker(_secondLatLng).addTo(map).bindPopup('Point B<br/>' + e.latlng + '<br/>' + e.layerPoint).openPopup();
//     }

//     if (_firstLatLng && _secondLatLng) {
//         L.polyline([_firstLatLng, _secondLatLng], {
//             color: 'blue'
//         }).addTo(map);

//         refreshDistanceAndLength();
//     }let routeLayer, dir;

//     function itinerary() {
//     dir = MQ.routing.directions();

//     routeLayer = MQ.routing.routeLayer({
//     directions: dir,
//     fitBounds: true,
//     ribbonOptions: {
//       draggable: false,
//       ribbonDisplay: { color: '#5882FA', opacity: 0.7 },
//       widths: [ 10, 15, 10, 15, 10, 13, 10, 12, 10, 11, 10, 11, 10, 12, 10, 14, 10 ]
//     },

//     altRibbonOptions: {
//       show: true,
//       ribbonDisplay: { color: '#F78181', opacity: 0.7},
//       hoverDisplay: { color: 'red', opacity: 0.6 }
//     }
//     });

//     dir.route({
//     maxRoutes: 5,let routeLayer, dir;

//     function itinerary() {
//     dir = MQ.routing.directions();

//     routeLayer = MQ.routing.routeLayer({
//     directions: dir,
//     fitBounds: true,
//     ribbonOptions: {
//       draggable: false,
//       ribbonDisplay: { color: '#5882FA', opacity: 0.7 },
//       widths: [ 10, 15, 10, 15, 10, 13, 10, 12, 10, 11, 10, 11, 10, 12, 10, 14, 10 ]
//     },

//     altRibbonOptions: {
//       show: true,
//       ribbonDisplay: { color: '#F78181', opacity: 0.7},
//       hoverDisplay: { color: 'red', opacity: 0.6 }
//     }
//     });

//     dir.route({
//     maxRoutes: 5,
//     timeOverage: 25,
//     locations: [
//       'chambery',
//       ' St Alban de Montbel'
//     ]
//     });

//     map.addLayer(routeLayer);

//     }
//     <!--Itinerary--> <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=nhwJdz7W5kO6QlcUXT8gTjVkWqSbz3Tp%22%3E</script> <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-routing.js?key=nhwJdz7W5kO6QlcUXT8gTjVkWqSbz3Tp%22%3E</script>
//     timeOverage: 25,
//     locations: [
//       'chambery',
//       ' St Alban de Montbel'
//     ]
//     });

//     map.addLayer(routeLayer);

//     }
//     <!--Itinerary--> <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-map.js?key=nhwJdz7W5kO6QlcUXT8gTjVkWqSbz3Tp%22%3E</script> <script src="https://www.mapquestapi.com/sdk/leaflet/v2.2/mq-routing.js?key=nhwJdz7W5kO6QlcUXT8gTjVkWqSbz3Tp%22%3E</script>
// });

// map.on('zoomend', function (e) {
//     refreshDistanceAndLength();
// })

// function refreshDistanceAndLength() {
//     _distance = L.GeometryUtil.distance(map, _firstLatLng, _secondLatLng);
//     _length = L.GeometryUtil.length([_firstPoint, _secondPoint]);
//     //   document.getElementById('distance').innerHTML = _distance;
//     //   document.getElementById('length').innerHTML = _length;
// };