function lines(url) {
  fetch(url)
    .then(function (response) {
      return response.json();
    })
    .then(response => { 
      for (let i = 0; i < response.records.length; i++) {  
        var coord = response.records[i].fields.geo_shape.coordinates;
        var coord_revers = coord.map(function (cord) {
          return cord.reverse();
        })

       let poly = L.polyline(coord_revers, {
            color: 'green'
          })
          .addTo(map);

          document.getElementById("reset").addEventListener("click", function () {
            map.removeLayer(poly);
          });
      };
    })
};
document.getElementById("lignes").addEventListener("click", function () {
  lines('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&facet=typeamenag&rows=306');
});

