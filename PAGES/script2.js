
//? NEW ICON

var iconred = L.icon({
    iconUrl: '/home/genoux/Projets/CARTO/Projet/cartographie/PAGES/IMAGES/Templatic-map-icons/places.png',
    
    iconSize:     [30, 45],
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

 
  //! function and fetch initiate

  function loadData(url, callback) {
    fetch(url)
      .then(function (response) {
        return response.json();
      })
      // parsed json data with fetch here
      .then(function (data) {
        callback(data);
      });
  }
  
  //! display only Chambery tickets informations
  
  function getDataCh(data) {
    // console.log (data)
    let properties = data.records;
  
    for (let i = 0; i < properties.length; i++) {
     let markerch = L.marker(properties[i].fields.localisation, {icon: iconred})
        markerch.bindPopup('<b>' + properties[i].fields.libelle + '</b>' + '<br/>' + properties[i].fields.adresse  + '<br/>' + properties[i].fields.commune)
        .addTo(map);
        // map.on('click', function () {
            document.getElementById("reset").addEventListener("click", function () {
          map.removeLayer(markerch);
        });
    }
  };
  
  
  document.getElementById("chamberybus").addEventListener("click", function () {
    loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=points-de-ventes-titres-de-transport-du-stac&facet=libelle&facet=commune&refine.commune=CHAMBERY', getDataCh);
  });
  
  