let map;

function init() {
  map = L.map('mapid').setView([45.564601, 5.917781], 14);
  L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);
};

window.addEventListener("load", function () {
  init()
});

// function and fetch initiate
function loadData(url, callback) {
  fetch(url)
    .then(function (response) {
      return response.json();
    })
    // parsed json data with fetch here
    .then(function (data) {
      callback(data);
    });
}

// display only Chambery informations
function getData(data) {
  // console.log (data)
  let properties = data.records;

  for (let i = 0; i < properties.length; i++) {
    L.marker(properties[i].fields.coordonnees_gps)
      .bindPopup('<b>' + properties[i].fields.nom_park + '<br/>' + properties[i].fields.adresse + '</b>' + '<br/>' + properties[i].fields.acces + '<br/>' + properties[i].fields.capacite)
      .addTo(map);
  }
};


document.getElementById("chambery").addEventListener("click", function () {
  loadData('https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&facet=delegataire&facet=secteur&facet=type_park&facet=remarque', getData);
});


document.getElementById("nothing").addEventListener("click", function () {
  alert("Aucune information ne répond à votre requête");
});